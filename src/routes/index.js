import {useState, useEffect} from 'react';
import axios from 'axios';
import { 
  BrowserRouter as Router, 
  Route,
  Redirect,
  useHistory
} from 'react-router-dom';

import EditPost from '../pages/EditPost';
import Detail from '../pages/Detail';
import FormLogin from '../pages/Forms/FormLogin';
import Nav from '../components/Navbar';
import Home from '../pages/Home';

const Routes = () => {

  const [login, setLogin] = useState(false);
  const [validateToken, setValidateToken] = useState();
  const [allPosts, setAllPosts] = useState([]);
  const [getPost, setGetPost] = useState({});
  
  const baseURL = 'https://jsonplaceholder.typicode.com';
  const history = useHistory();
  //Función que hace la peticion del token
  const getToken = async (data) => {
    try {
      const getToken = await axios.post('http://challenge-react.alkemy.org/', data);
      const token = await getToken.data;
      await localStorage.setItem('token', JSON.stringify(token));
      setValidateToken(JSON.parse(localStorage.getItem('token')));
      history.push('/');
    } catch (error) {
      console.log(error);
    }
  }


  //Función que va a buscar todas las entradas del blog

    const getPostFromApi = async () => {
      try {
        const getResp = await axios.get(`${baseURL}/posts`);
        const getPosts = await getResp.data;
        localStorage.setItem('allPosts', JSON.stringify(getPosts));
        setAllPosts(JSON.parse(localStorage.getItem('allPosts')));
        setValidateToken(JSON.parse(localStorage.getItem('token')));
      } catch (error) {
        console.log(error);
      }
    }

  //Function que realiza una petición post para cargar una nueva entrada en el blog
  const addPostFromApi = async (newData) => {
    try {
      const sendData = await axios.post(`${baseURL}/posts`, newData);
      const data = await sendData.data;
      setAllPosts([...allPosts, data]);
    } catch (error) {
      console.log(error)
    }
  }

  //Función que busca una entrada según su id
  const getPostForId = async (id) => {
    try {
      const getId = await axios.get(`${baseURL}/posts/${id}`);
      const post =  await getId.data;
      await setGetPost(post);
    } catch (error) {
      console.log(error)
    }
  }

  //Función para modificar algun elemento de una entrada
  const patchPostForId = async (id, dataChange) => {
    try {
      const idToChange = await axios.patch(`${baseURL}/posts/${id}`, dataChange)
      const data = await idToChange.data;
      console.log(idToChange);
      console.log(data)
    } catch (error) {
      console.log(error)
    }
  }

  const deletePostForId = async (id) => {
    try {
      const idToDelete = await axios.delete(`${baseURL}/posts/${id}`)
      console.log(idToDelete)
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    getPostFromApi();
  }, [])
  

  return ( 
    <Router>
      <Nav validateToken={validateToken} />
        <Route path='/' exact>
          {validateToken !== null || login === true ? 
            <Home 
            allPosts={allPosts}
            addPostFromApi={addPostFromApi}
            getPostForId={getPostForId}
            deletePostForId={deletePostForId}
          /> : 
          <Redirect to='/login' /> 
        }
        </Route>

        <Route path='/edit'>
          {validateToken !== null || login === true ? 
            <EditPost 
              getPost={getPost}
              patchPostForId={patchPostForId}
            /> :
            <Redirect to='/login' /> 
          }
        </Route>

        <Route path='/detail'>
            <Detail 
              getPost={getPost}
            />
        </Route>

        <Route path='/login'>
          <FormLogin 
            getToken={getToken} 
            setLogin={setLogin}
          />
        </Route>

    </Router>
  );
}
 
export default Routes;