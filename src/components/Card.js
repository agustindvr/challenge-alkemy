import {useHistory} from 'react-router-dom';

import {Card, Button} from 'react-bootstrap';

const CardPost = ({title, id, getPostForId, deletePostForId}) => {

  const history = useHistory();

  const goToEdit = async (id) => {
   await getPostForId(id);
    history.push('/edit')
  }

  const goToDetail = async (id) => {
    await getPostForId(id);
    history.push('/detail')
  }

  const deletePost = async (id) => {
    await deletePostForId(id);
  }

  return ( 
      <Card style={{ width: '97%', height: '100%' }}  className='m-2 border_card' border='success' >
        <Card.Body className='p-5'>
          <Card.Title className='text-uppercase'>{title}</Card.Title>
        </Card.Body>
        <Card.Footer>
        <div className='d-flex flex-row flex-wrap justify-content-around'>
          <Button className='my-2' variant="success" onClick={() => goToEdit(id)} >Editar</Button>
          <Button className='my-2' variant="primary" onClick={() => goToDetail(id)} >Detalle</Button>
          <Button className='my-2' variant="dark" onClick={() => deletePost(id)} >Eliminar</Button>
        </div>
        </Card.Footer>
      </Card>
  );
}
 
export default CardPost;