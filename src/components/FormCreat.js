import { Formik } from 'formik';
import * as Yup from 'yup';
import { nanoid } from 'nanoid';

import { Form, FormControl, Alert, Button} from 'react-bootstrap';

const newPostSchema = Yup.object().shape({
  title: Yup.string().required('Este campo es obligatorio'),
  body: Yup.string().required('Este campo es obligatorio'),
})

const FormCreate = ({addPostFromApi, handleClose, labelTitle, labelMessage}) => {

  const addNewPost = (e) => {
    e.preventDefault()

    const [title, body, id] = e.target.elements

    const newData = {
      title: title.value,
      body: body.value,
      id: id.value,
    }

    addPostFromApi(newData)
    handleClose()
  }

  return ( 
    <Formik 
    initialValues={{title: '', body:'', id: nanoid()}}
    validationSchema={newPostSchema}
    validateOnBlur
  >
    {({handleBlur, handleChange, values, errors, touched}) => (
      <Form onSubmit={addNewPost}>
        <Form.Label  className='fs-3'>{labelTitle}</Form.Label>
      <FormControl 
        className='my-4 responsive_elements text-center mx-auto'
        type='text'
        placeholder='Ingrese un Titulo'
        name='title'
        onBlur={handleBlur('title')}
        value={values.title}
        onChange={handleChange('title')}
      />
      {touched.title && errors.title && <Alert variant='danger'>{errors.title}</Alert>}
        <Form.Label  className='fs-3'>{labelMessage}</Form.Label>
      <Form.Control 
      as='textarea'
      className='my-4 responsive_elements text-center mx-auto'
      type='text'
      placeholder='Ingrese '
      name='body'
      onBlur={handleBlur('body')}
      value={values.body}
      onChange={handleChange('body')}
      />
      {touched.body && errors.body && <Alert variant='danger'>{errors.body}</Alert>}
      <Button type='submit' variant="secondary">
        Guardar
      </Button>
    
      </Form>
    )}
  </Formik>
   );
}
 
export default FormCreate;