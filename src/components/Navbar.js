import {Link, Redirect} from 'react-router-dom';

import {Container, Row, Col} from 'react-bootstrap';

const Nav = ({validateToken}) => {

  return ( 
    <>
    <Container fluid className='navbar_container' >
      <Row>
        <Col>
            <Link to='/' className=' btn btn-danger'>Home</Link>
        </Col>
        <Col>
          <Link to='/edit' className='btn btn-warning'>Edit</Link>
        </Col>
      </Row>
    </Container>
      {validateToken === null ? <Redirect to='/login' /> : null }
    </>
   );
}
 
export default Nav;