import {useState} from 'react';

import CardPost from '../components/Card';

import {Container, Row, Col, Button, Modal} from 'react-bootstrap';
import FormCreate from '../components/FormCreat';

const Home = ({allPosts, addPostFromApi, getPostForId, deletePostForId}) => {

  const [showModal, setShowModal] = useState(false);

  const handleClose = () => setShowModal(false);
  const handleShow = () => setShowModal(true);

  return ( 
    <Container fluid>
      <Row className='text-center d-flex flex-row- flex-wrap gap-4' >
        { 
          allPosts.map((post) => (
              <Col key={post.id} >
                <CardPost title={post.title} body={post.body} id={post.id} getPostForId={getPostForId} deletePostForId={deletePostForId} />
              </Col>)
            )
          }
          </Row>
        <Button 
          className='rounded-circle fixed-bottom right-0 my-2 mx-2 bg-success border-success' 
          type='button'
          onClick={handleShow}
        >+</Button>

        <Modal show={showModal} onHide={handleClose} className='bg_modal text-dark text-center'>
          <Modal.Header>
            <Modal.Title>Nuevo Post!</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <FormCreate 
              addPostFromApi={addPostFromApi} 
              handleClose={handleClose} 
              labelTitle='Titulo del post' 
              labelMessage='Deje su mensaje aqui' 
            />
          </Modal.Body>
        </Modal>
    </Container>
  );
}
 
export default Home;