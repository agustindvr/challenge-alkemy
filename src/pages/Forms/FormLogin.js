import {useHistory} from 'react-router-dom'
import { Formik } from 'formik';
import * as Yup from 'yup';

import {Form ,FormControl, Button, Container, Alert} from 'react-bootstrap';

const LoginSchema = Yup.object().shape({
  email: Yup.string().email('Este correo electrónico es inválido').required('Este campo es obligatorio'),
  password: Yup.string().required('Este campo es obligatorio')
})

const FormLogin = ({getToken, setLogin}) => {

  const history = useHistory();

  const createPost = (e) => {
    e.preventDefault()
    const [email, password] = e.target.elements

    const data = {
      email: email.value,
      password: password.value
    }

    getToken(data);   
    setLogin(true);
    history.push('/');
  }


  return ( 
    <>
      <Container className='responsive_form text-center my-5 mx-auto'>
        <Formik 
          initialValues={{email: '', password: ''}}
          validationSchema = {LoginSchema}
          validateOnBlur
        >
          { ({handleBlur, handleChange, values, errors, touched}) => (
            <div className='container_form'>
              <Form  onSubmit={createPost} >
                <FormControl 
                  className='my-4 responsive_elements text-center mx-auto'
                  type='email'
                  placeholder='Ingrese Email'
                  name='email'
                  onBlur={handleBlur('email')}
                  value={values.email}
                  onChange={handleChange('email')}
                />
                {touched.email && errors.email && <Alert variant='danger'>{errors.email}</Alert>}
                <FormControl 
                className='my-4 responsive_elements text-center mx-auto'
                type='password'
                placeholder='Password'
                name='password'
                onBlur={handleBlur('password')}
                value={values.password}
                onChange={handleChange('password')}
                />
                {touched.password && errors.password && <Alert variant='danger'>{errors.password}</Alert>}
                <Button type='submit' className='btn btn-danger responsive_elements my-4'>Enviar</Button>
                </Form>
            </div>
            )
          }
        </Formik>
      </Container>
    </>
  );
}

export default FormLogin;