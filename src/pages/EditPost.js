import {useState} from 'react';

import {Formik} from 'formik';
import * as Yup from 'yup';
import {Form, FormControl, Alert, Button} from 'react-bootstrap';


const EditPost = ({getPost, patchPostForId}) => {
  const [dataChange, setDataChange] = useState({})
  
  const changedata = async (e) => {
    e.preventDefault();

    const [title, body] = e.target.elements;

    await setDataChange({
      title: title.value,
      body: body.value
    })

    await patchPostForId(getPost.id, dataChange)

  }

  const editPostSchema = Yup.object().shape({
    title: Yup.string().required('Este campo es obligatorio'),
    body: Yup.string().required('Este campo es obligatorio'),
  })

  return (
    <Formik 
              initialValues={{title: getPost.title, body:getPost.body}}
              validationSchema={editPostSchema}
              validateOnBlur
            >
              {({handleBlur, handleChange, values, errors, touched}) => (
                <Form onSubmit={changedata} className='text-center border_card mt-5 mx-2'>
                  <Form.Label className='fs-2 mt-3'>Nuevo título</Form.Label>
                <FormControl 
                  className='my-4 responsive_elements text-center mx-auto'
                  type='text'
                  placeholder='Ingrese un Titulo'
                  name='title'
                  onBlur={handleBlur('title')}
                  value={values.title}
                  onChange={handleChange('title')}
                />
                {touched.title && errors.title && <Alert variant='danger'>{errors.title}</Alert>}
                  <Form.Label className='fs-2'>Nuevo mensaje</Form.Label>
                <Form.Control 
                as='textarea'
                className='my-4 responsive_elements text-center mx-auto'
                type='text'
                placeholder='Ingrese '
                name='body'
                onBlur={handleBlur('body')}
                value={values.body}
                onChange={handleChange('body')}
                />
                {touched.body && errors.body && <Alert variant='danger'>{errors.body}</Alert>}
                <Button type='submit' variant="secondary" className='mb-2 pb-2'>
                  Guardar
                </Button>
              
                </Form>
              )}
            </Formik>
  )
}
 
export default EditPost;