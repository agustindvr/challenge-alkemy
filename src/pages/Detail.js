import {Container, Row, Col, Card} from 'react-bootstrap';

const Detail = ({getPost}) => {

  console.log(getPost)

  const {title, body, id, userId} = getPost

  return ( 
    <Container>
      <Row>
        <Col className='text-center'>
          <Card style={{ width: '100%' }}  className='m-2 border_card' border='success' >
            <Card.Body className='p-5'>
              <Card.Title className='text-uppercase'>{title}</Card.Title>
              <Card.Text className='my-3 border_text_card p-3'>
                {body}
              </Card.Text>
              <Card.Text className='justify-content-between'><p>userId: {userId} id:{id}</p> </Card.Text>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>

   );
}
 
export default Detail;